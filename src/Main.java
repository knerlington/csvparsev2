
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Main {
	private static final String filepath = "test_data_java_exercise.csv";
	private static ArrayList<String> processTypes, measureTypes; /*helps tracking known process types, measures from csv*/
	private static ArrayList<Record> records; /*object representing row with mean val, nr of errors from rows in csv*/ 


	public static void main(String[] args) throws FileNotFoundException {
		//initialization 
		processTypes = new ArrayList<String>();
		measureTypes = new ArrayList<String>();
		records = new ArrayList<Record>();

		/*create buffered reader capable of counting lines*/
		LineNumberReader reader = null;
		String line = ""; //line in csv file
		String separator = ";"; //line separator
		try{
			reader = new LineNumberReader(new FileReader(filepath));
			while((line = reader.readLine()) != null){

				if(reader.getLineNumber()> 1){
					//skip first line since it contains only headers
					String[] columns = line.split(separator);

					//save process and measure types
					if(!processTypes.contains(columns[0])){
						//process unknown, add to list
						processTypes.add(columns[0]);
						Record record = new Record(columns[0]);
						records.add(record);
					}else{
						//process known, get record of same type
						for(Record r:records){

							if(r.type.equals(columns[0])){
								//right record found

								//check if ERROR
								if(columns[2].equals("ERROR")){
									//check if known error
									if(r.getErrorSet().containsKey(columns[1])){
										//known error
										for(Map.Entry<String, Integer> errorMap:r.getErrorSet().entrySet()){
											if(errorMap.getKey().equals(columns[1])){
												//right error measure found ++
												int i = errorMap.getValue();
												i++;
												errorMap.setValue(i);
											}

										}
									}else{
										//unknown error
										r.getErrorSet().put(columns[1], 1);


									}
								}
								//check if real val
								if(!columns[2].equals("ERROR")){
									//check if known measure
									if(r.getMeasureSet().containsKey(columns[1])){
										//known measure add val
										for(Map.Entry<String, Float> map:r.getMeasureSet().entrySet()){
											if(map.getKey().equals(columns[1])){
												Float f = map.getValue();
												f+=Float.parseFloat(columns[2]);
												map.setValue(f);
											}

										}
										//increment nr of times measure occurred
										for(Map.Entry<String, Integer> map:r.getMeasureCounter().entrySet()){
											if(map.getKey().equals(columns[1])){
												int val = map.getValue();
												val++;
												map.setValue(val);
											}
										}
									}else{
										//unknown measure
										r.getMeasureSet().put(columns[1],Float.parseFloat(columns[2]));
										r.getMeasureCounter().put(columns[1], 1);

									}
								}
							}
						}
					}


				}
			}
			//calc mean val for each measure for each record
			for(Record r:records){
				r.calcMeanVals();
			}
			//determine header column order and print results
			determineColumnOrder();
		}catch(IOException e){

		}
	}
	/*
	 * Java method to sort Map in Java by value e.g. HashMap or Hashtable
	 * throw NullPointerException if Map contains null values
	 * It also sort values even if they are duplicates
	 */
	public static <K extends Comparable,V extends Comparable> Map<K,V> sortByValues(Map<K,V> map){
		List<Map.Entry<K,V>> entries = new LinkedList<Map.Entry<K,V>>(map.entrySet());

		Collections.sort(entries, new Comparator<Map.Entry<K,V>>() {

			@Override
			public int compare(Entry<K, V> o1, Entry<K, V> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		});

		//LinkedHashMap will keep the keys in the order they are inserted
		//which is currently sorted on natural ordering
		Map<K,V> sortedMap = new LinkedHashMap<K,V>();

		for(Map.Entry<K,V> entry: entries){
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}


	public static void determineColumnOrder(){
		Map<String, Integer> highestErrorVals = new TreeMap<String, Integer>(); //measures with most errors 
		//calc which col for all records have the most errors
		for(Record r:records){
			for(Map.Entry<String, Integer> entry:r.getErrorSet().entrySet()){
				if(!highestErrorVals.containsKey(entry.getKey())){
					highestErrorVals.put(entry.getKey(), entry.getValue());
				}else{
					for(Map.Entry<String, Integer> me:highestErrorVals.entrySet()){
						if(me.getKey().equals(entry.getKey())){
							if(entry.getValue() > me.getValue()){
								me.setValue(entry.getValue());
							}

						}
					}
				}
			}

		}
		//sort map with cols that have the most errors
		List<String> sortedList = new ArrayList<String>(sortByValues(highestErrorVals).keySet());
		//create header list
		List<String> allCols = new ArrayList<String>();
		allCols.add("Type");
		for(int i = sortedList.size()-1; i >=0;i--){
			allCols.add("Errors "+sortedList.get(i));
			allCols.add("AVG "+sortedList.get(i));
		}

		//print headers
		for(String s:allCols){
			System.out.printf("%1$-13s | ",s);
		}
		//print results
		for(Record r:records){
			String line = r.type;
			System.out.println();
			System.out.printf("%1$-13s | ",r.type);
			for(int i = sortedList.size()-1; i >=0;i--){
				System.out.printf("%1$-14s | %2$-14s | ",r.getErrorSet().get(sortedList.get(i)),r.getMeasureMeanVals().get(sortedList.get(i)));

			}
		}

	}
}

