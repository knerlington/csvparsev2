

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Record {
	public String type; //type of process
	private Map<String,Float> measureSet; //set with each avg measure-value from db
	private Map<String,Integer> errorSet; //set with each avg measure-error count from db
	private Map<String,Integer> measureCounter; //set with each avg measure-error count from db
	private Map<String,Float> measureMeanVals; //set with each avg measure-val from db
	
	public Map<String, Float> getMeasureMeanVals() {
		return measureMeanVals;
	}

	public Map<String, Integer> getMeasureCounter() {
		return measureCounter;
	}

	public int measureNr = 0;
	
	public Map<String, Float> getMeasureSet() {
		return measureSet;
	}

	public Map<String, Integer> getErrorSet() {
		return errorSet;
	}

	public String getType() {
		return type;
	}

	public Record(String type) {
		this.type = type;
		this.measureSet = new HashMap<String, Float>();
		this.errorSet = new HashMap<String, Integer>();
		this.measureCounter= new HashMap<String, Integer>();
		this.measureMeanVals = new HashMap<String, Float>();
	}
	public void calcMeanVals(){
		for(Map.Entry<String, Integer> counts:this.getMeasureCounter().entrySet()){
			for(Map.Entry<String, Float> vals:this.getMeasureSet().entrySet()){
				if(vals.getKey().equals(counts.getKey())){
					Float val = vals.getValue()/counts.getValue();
					this.measureMeanVals.put(vals.getKey(), val);
				}
			}
		}
	}
/*	public Map<String, Integer> sortErrors(){
		  Map<String, Integer> map = new TreeMap<String, Integer>(this.getErrorSet());
		  Map<String, Integer> results = new LinkedHashMap<String, Integer>(); //LinkedHashMap preserves order
	         Set set = map.entrySet();
	         Iterator iterator = set.iterator();
	         
	         while(iterator.hasNext()) {
	              Map.Entry me = (Map.Entry)iterator.next();
	              results.put((String)me.getKey(), (int)me.getValue());
	              System.out.print(me.getKey() + ": ");
	              System.out.println(me.getValue());
	         }
	         return results;
	}*/



}


